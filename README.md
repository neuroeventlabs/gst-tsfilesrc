# Timestamped file source GStreamer plugin

This is a GStreamer element for feeding timestamped files to a pipeline. It is
intended to be used in a similar fashion as multifilesrc, with the main
exception that the file name matching pattern is based on timestamps, not
simply file names. This means that a video stream can be created from a
set of images with non-fixed frame durations or dropped frames.

## Usage

Once installed, you can use the tsfilesrc plugin like [multifilesrc](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good-plugins/html/gst-plugins-good-plugins-multifilesrc.html).
Assuming you have a number of files named `image-<timestamp_seconds.fraction>.png` in
your directory, you can display them with the following command:

    tsfilesrc location=image-%f.png ! pngdec ! videoconvert ! xvimagesink

Another example, encoding to an MP4 file:

    tsfilesrc location=%f.png ! pngdec ! videoconvert ! x264enc ! mp4mux ! filesink location=output.mp4

Note that the timestamp format is in decimal (floating point) seconds, so
fractions of a second come after the decimal separator. If your locale does not
match the decimal separator used in the filename, you may use the
`--decimal-separator` property to pass a different separation character.

## Installation

### Pre-built binaries

Precompiled binaries for windows can be found [here](https://gitlab.com/neuroeventlabs/gst-tsfilesrc/wikis/Releases).
Once downloaded, follow the instructions below to install.

### Copying the plugin to GStreamer's plugins directory

#### Linux

Install GStreamer using your favorite method (e.g., from the package manager). Then:

    <sudo> cp libgst-tsrealsense.so /usr/lib/gstreamer-1.0/

#### Windows

Install [GStreamer for Windows from the official release](https://gstreamer.freedesktop.org/data/pkg/windows/).

More specifically, install the non-developer x86_64 package for the latest
version of gstreamer. Then copy the DLL you built or downloaded to the gstreamer
plugins directory:

    cp libgst-tsrealsense.dll C:\gstreamer\1.0\x86_64\lib\x86_64\gstreamer-1.0


## Compilation

### All platforms

- Clone this repository.

- Install [Qbs](https://doc.qt.io/qbs/index.html). Most desktop Linux distributions
have a qbs package in their repositories. The Qt Project offers prebuilt packages
of Qt Creator, which comes with Qbs.

### Linux

Install GStreamer development packages for your distro. In practice, this
means the following command

    pkg-config --modversion gstreamer-base-1.0

should output something like

    1.12.2

Once that has been done, you can build the plugin with:

    qbs qbs.installRoot:/path/of/your/choice

Then, copy the resulting binary to GStreamer plugins directory or use
GST_PLUGIN_PATH to load the plugin from a path of your choice.


### Windows

Coming soon.
