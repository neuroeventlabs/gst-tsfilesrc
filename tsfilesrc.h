/* Timestamped file source plugin for GStreamer
 * Copyright (C) 2017 Andrew Knight, Neuro Event Labs Oy <andrew@neuroeventlabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _TSFILESRC_H_
#define _TSFILESRC_H_

#include <gst/base/gstpushsrc.h>

G_BEGIN_DECLS

struct GstTsFileSrc;
struct GstTsFileSrcClass;

#define GST_TYPE_TSFILESRC (gst_tsfilesrc_get_type())
#define GST_TSFILESRC(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_TSFILESRC, GstTsFileSrc))
#define GST_TSFILESRC_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_TSFILESRC, GstTsFileSrcClass))
#define GST_IS_TSFILESRC(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_TSFILESRC))
#define GST_IS_TSFILESRC_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_TSFILESRC))

GType gst_tsfilesrc_get_type();

G_END_DECLS

#endif // _TSFILESRC_H_
