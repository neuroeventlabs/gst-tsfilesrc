import qbs
import "qbs/imports/PkgconfigProduct.qbs" as Import

Project {
  Import { name: "gstreamer-base-1.0" }

  DynamicLibrary {
    name: "gst-tsfilesrc"

    cpp.cxxLanguageVersion: "c++14"
    cpp.treatWarningsAsErrors: true

    Properties {
      condition: qbs.targetOS.contains("windows")
      cpp.cxxFlags: ["-fno-sized-deallocation"]
      cpp.defines: ["_GLIBCXX_USE_CXX11_ABI=0"]
      cpp.enableExceptions: false
    }

    Depends { name: "cpp" }
    Depends { name: "gstreamer-base-1.0" }

    files: [
      "README.md",
      "tsfilesrc.cpp",
      "tsfilesrc.h",
    ]

    Group {
      fileTagsFilter: "dynamiclibrary"
      qbs.install: true
    }
  }
}
