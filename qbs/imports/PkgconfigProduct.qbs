import qbs
import qbs.Probes

Product {
  condition: probe.found
  property bool useSystem: true

  Probes.PkgConfigProbe {
    id: probe
    name: product.name
    libDirs: product.useSystem ? undefined
      : [project.sourceDirectory + "/node_modules/lib/pkgconfig"]
  }

  Export {
    Depends { name: "cpp" }
    cpp.defines: probe.defines
    cpp.dynamicLibraries: probe.libraries
    cpp.libraryPaths: probe.libraryPaths
    cpp.includePaths: probe.includePaths
    cpp.rpaths: probe.libraryPaths ? probe.libraryPaths.map(function (libraryPath) {
      return libraryPath.replace(project.sourceDirectory, "$ORIGIN");
    }) : []
  }
}
