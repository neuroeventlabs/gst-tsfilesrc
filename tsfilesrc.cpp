/* Timestamped file source plugin for GStreamer
 * Copyright (C) 2017 Andrew Knight <andrew.knight@neuroeventlabs.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-tsfilesrc
 *
 * The tsfilesrc element works similarly to multifilesrc, but extracts the
 * timestamp of an image frame frame based on the name of the file.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 tsfilesrc location=%f.png ! pngdec ! videoconvert ! xvimagesink
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <algorithm>
#include <locale>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include <gst/gst.h>
#include <gst/base/gstpushsrc.h>
#include "tsfilesrc.h"

#define S_TO_NS 1e9

#define DEFAULT_LOCATION "%f.png"

static char getLocaleDecimalSeparator() {
  const std::locale locale("");
  return std::use_facet<std::numpunct<char>>(locale).decimal_point();
}

struct CustomDecimalFacet : public std::numpunct<char> {
  char decimal_point;

  CustomDecimalFacet(char decimal_point)
    : decimal_point(decimal_point) {
  }

  char do_decimal_point() const override {
    return decimal_point;
  }
};

struct GstTsFileSrc {
  struct Location {
    std::string path;
    double timestamp;
  };

  GstPushSrc base;

  struct Data {
    std::string locationPattern = DEFAULT_LOCATION;
    std::vector<Location> locations;
    std::vector<Location>::const_iterator locationIterator;
    char decimalSeparator = getLocaleDecimalSeparator();
    bool locationDirty = true;
  };
  Data *d;

  void setLocation(const char *location) {
    d->locationPattern = location;
    d->locationDirty = true;
  }

  void setDecimalSeparator(char separator) {
    d->decimalSeparator = separator;
    d->locationDirty = true;
  }

  Location nextLocation() {
    if (d->locationDirty) {
      enumerateLocations();
      d->locationDirty = false;
    }

    if (d->locationIterator == d->locations.end()) {
      return Location { "", 0 };
    }

    return *(d->locationIterator++);
  }

  void enumerateLocations() {
    d->locations = std::vector<Location>();

    std::string::size_type timestampPosition;
    {
      auto basename = g_path_get_basename(d->locationPattern.c_str());
      timestampPosition = std::string(basename).find("%f");
      g_free(basename);

      if (timestampPosition == std::string::npos) {
        std::cerr << "No %f parameter was found in the location pattern. "
                     "Check the location property." << std::endl;
        return;
      }
    }

    GError *error = nullptr;
    auto dirname = g_path_get_dirname(d->locationPattern.c_str());
    auto path = g_dir_open(dirname, 0, &error);
    if (error) {
      GST_ELEMENT_ERROR(this, RESOURCE, READ,
        ("Error opening directory \"%s\".", dirname),
        ("%s", error->message));
      g_error_free(error);
    }
    if (!path) {
      return;
    }

    std::locale locale( // locale takes ownership of facet
      std::locale(""), new CustomDecimalFacet(d->decimalSeparator));
    while (auto entry = g_dir_read_name(path)) {
      // TODO: check regex of basename to make sure it matches pattern

      double timestamp;
      std::istringstream stream(entry);
      stream.imbue(locale);
      stream.seekg(timestampPosition);
      stream >> timestamp;
      if (!timestamp) {
        continue;
      }

      d->locations.push_back({ std::string(dirname) + '/' + entry, timestamp });
    }
    g_free(dirname);

    std::sort(d->locations.begin(), d->locations.end(), [](const auto &a, const auto &b) {
      return a.timestamp < b.timestamp;
    });

    g_dir_close(path);
    d->locationIterator = d->locations.begin();
  }

  double startTime() const {
    if (d->locations.empty()) {
      return 0;
    }

    return d->locations.front().timestamp;
  }
};

struct GstTsFileSrcClass {
  GstPushSrcClass base;
};

enum {
  PROP_0,
  PROP_LOCATION,
  PROP_DECIMAL_SEPARATOR,
};

GST_DEBUG_CATEGORY_STATIC(tsfilesrcDebugCategory);
#define GST_CAT_DEFAULT tsfilesrcDebugCategory

G_DEFINE_TYPE_WITH_CODE(
  GstTsFileSrc, gst_tsfilesrc, GST_TYPE_PUSH_SRC,
  GST_DEBUG_CATEGORY_INIT(tsfilesrcDebugCategory, "tsfilesrc", 0,
  "debug category for tsfilesrc element"))

static void gst_tsfilesrc_class_init(GstTsFileSrcClass *elementClass) {
  auto gobjectClass = G_OBJECT_CLASS(elementClass);
  auto pushSrcClass = GST_PUSH_SRC_CLASS(elementClass);

  static GstStaticPadTemplate sourceTemplate = GST_STATIC_PAD_TEMPLATE(
    "src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

  gst_element_class_add_static_pad_template(
    GST_ELEMENT_CLASS(elementClass),
    &sourceTemplate);

  gst_element_class_set_static_metadata(GST_ELEMENT_CLASS(elementClass),
    "Timestamped image file source",
    "Source/File", "Generate a video stream from a series of timestamped image frames.",
    "Andrew Knight <andrew.knight@neuroeventlabs.com>");

  gobjectClass->set_property = [](GObject *base, guint property, const GValue *value, GParamSpec *spec) {
    GstTsFileSrc *src = GST_TSFILESRC(base);
    GST_DEBUG_OBJECT(src, "set_property");

    switch (property) {
    case PROP_LOCATION: {
      src->setLocation(g_value_get_string(value));
      break;
    }
    case PROP_DECIMAL_SEPARATOR: {
      src->setDecimalSeparator(g_value_get_string(value)[0]);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(src, property, spec);
      break;
    }
  };

  gobjectClass->get_property = [](GObject *base, guint property,  GValue *value, GParamSpec *spec) {
    GstTsFileSrc *src = GST_TSFILESRC(base);
    GST_DEBUG_OBJECT(src, "get_property");

    switch (property) {
    case PROP_LOCATION:
      g_value_set_string(value, src->d->locationPattern.c_str());
      break;
    case PROP_DECIMAL_SEPARATOR:
      g_value_set_string(value, std::string(1, src->d->decimalSeparator).c_str());
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(src, property, spec);
      break;
    }
  };

  g_object_class_install_property(gobjectClass, PROP_LOCATION, g_param_spec_string(
    "location", "File Location",
    "Pattern to find input files. The %f placeholder is interpreted as the timestamp "
    "of the video stream in seconds. The lowest timestamp is considered the start time. "
    "Note that the decimal separator is that of the current locale, unless modified "
    "with the decimal-separator property.",
    DEFAULT_LOCATION, GParamFlags(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  std::locale locale("");
  g_object_class_install_property(gobjectClass, PROP_DECIMAL_SEPARATOR, g_param_spec_string(
    "decimal-separator", "Decimal Separator",
    "Character to treat as the decimal separator when evaluating the timestamp.",
    std::string(1, getLocaleDecimalSeparator()).c_str(),
    GParamFlags(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  gobjectClass->dispose = [](GObject *base) {
    auto src = GST_TSFILESRC(base);
    GST_DEBUG_OBJECT(src, "dispose");
    delete src->d;

    G_OBJECT_CLASS(gst_tsfilesrc_parent_class)->dispose(base);
  };

  gobjectClass->finalize = [](GObject *base) {
    auto src = GST_TSFILESRC(base);
    GST_DEBUG_OBJECT (src, "finalize");
    /* clean up object here */
    G_OBJECT_CLASS(gst_tsfilesrc_parent_class)->finalize(base);
  };

  pushSrcClass->create = [](GstPushSrc *base, GstBuffer **target) {
    auto src = GST_TSFILESRC(base);
    auto location = src->nextLocation();
    if (location.path.empty()) {
      return GST_FLOW_EOS;
    }

    // Return the contents of each file.
    gchar *data;
    gsize length;
    GError *error = nullptr;
    if (!g_file_get_contents(location.path.c_str(), &data, &length, &error)) {
      if (error) {
        GST_ELEMENT_ERROR(src, RESOURCE, READ,
          ("Error while reading from file \"%s\".", location.path.c_str()),
          ("%s", error->message));
        g_error_free(error);
      }
      return GST_FLOW_ERROR;
    }

    auto &buffer = *target;
    buffer = gst_buffer_new();
    gst_buffer_append_memory(
      buffer, gst_memory_new_wrapped(GST_MEMORY_FLAG_READONLY, data, length, 0, length, data, g_free));
    buffer->pts = (location.timestamp - src->startTime()) * S_TO_NS;

    return GST_FLOW_OK;
  };
}

static void gst_tsfilesrc_init(GstTsFileSrc *src) {
  src->d = new GstTsFileSrc::Data;
}

#ifndef VERSION
#define VERSION "0.0.2"
#endif
#ifndef PACKAGE
#define PACKAGE "tsfilesrc"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "tsfilesrc"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "https://neuroeventlabs.com"
#endif

GST_PLUGIN_DEFINE(
  GST_VERSION_MAJOR, GST_VERSION_MINOR, tsfilesrc, "Timestamped file source plugin",
  [](GstPlugin *plugin) { return gst_element_register(plugin, "tsfilesrc", GST_RANK_NONE, GST_TYPE_TSFILESRC); },
  VERSION, "MIT/X11", PACKAGE_NAME, GST_PACKAGE_ORIGIN)
